import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeInfo } from './welcome/welcome.component';
import { Employee } from './employee/employee.component';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private baseURL = "http://localhost:8080/welcome";
  private headers = new HttpHeaders ({'content-type':'application/json'});

  myURL:string;
  dataInEmployeeService: string;
  error: string;

  constructor(private http:HttpClient) { }

  isValidUser():Observable<any>{
    this.myURL = this.baseURL + "/isValidUser?UserID=DebaReDeba&PassWord=Jyoti";
    console.log(this.myURL);
    return this.http.get(this.myURL, {responseType:'text'});
  }

  getEmployee():Observable<any>{
    let employeeObj = new EmployeeInfo(1234,"Debajyoti", "ADM","PAT");
    this.baseURL = this.baseURL +"/getEmployee";
    return this.http.post(this.baseURL, employeeObj);
  }

  getEmployees():Observable<any>{
    let url = this.baseURL +"/getEmployees";
    return this.http.get(url);
  }

  sendEmployee(employeeObj:Employee):Observable<any>{
    let url = this.baseURL +"/registerEmployee";
    return this.http.post(url,employeeObj);
  }
  fetchEmployees():Observable<any>{
    let url = this.baseURL +"/fetchEmployees";
    return this.http.get(url);
  }

  deleteEmployees(id:number):Observable<any>{
    let url = this.baseURL +"/deleteEmployee";
    console.log(url+'/'+id)
    return this.http.delete(url+'/'+id);
  }

  updateEmployees(employeeObj:Employee):Observable<any>{
    console.log(employeeObj)
    let url = this.baseURL +"/updateEmployee";
    return this.http.put(url,employeeObj);
  }
}