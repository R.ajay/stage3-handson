import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  dataString:string;
  employeeObj:EmployeeInfo;
  employeeList:EmployeeInfo[];
  errorMsg:string;
  constructor(private employeeService:EmployeeService) { }

  ngOnInit(): void {
  }

  ping1(){
    this.employeeService.isValidUser().subscribe(
      data=>
      {
        this.employeeObj = data;
        this.dataString = "Employee id : "+this.employeeObj.id+ ", Name: "+this.employeeObj.name+ ", Department: "+this.employeeObj.dept+", Designation: "+this.employeeObj.designation;
        alert(this.dataString);
        console.log(this.dataString);
      },
      (error)=>{this.errorMsg = error;alert(this.errorMsg)}
    )
  }
  ping2(){
    this.employeeService.getEmployee().subscribe(
      data=>
      {
        this.employeeObj = data;
        this.dataString = "Employee id : "+this.employeeObj.id+ ", Name: "+this.employeeObj.name+ ", Department: "+this.employeeObj.dept+", Designation: "+this.employeeObj.designation;
        alert(this.dataString);
        console.log(this.dataString);
      },
      (error)=>{this.errorMsg = error;alert(this.errorMsg)}
    )
  }
  ping3(){
    this.employeeService.getEmployees().subscribe(
      data=>
      {
        this.employeeList = data;
        this.employeeList.forEach(
          function(employeeObj){
            data = "Employee id : "+employeeObj.id+ ", Name: "+employeeObj.name+ ", Department: "+employeeObj.dept+", Designation: "+employeeObj.designation;    
            alert(data);
          }
        )

      },
      (error)=>{this.errorMsg = error;alert(this.errorMsg)}
    )
  }
}

export class EmployeeInfo{
  id: number;
  name: string;
  dept: string;
  designation: string;

  constructor(id: number, name: string, dept: string, designation: string){
    this.id = id;
    this.name = name;
    this.dept = dept;
    this.designation = designation;
  }
}