import { Component, OnInit } from '@angular/core';
import { CompanyStock } from '../company-stock';
import { EventEmitterService } from '../event-emitter.service';
import { CompanyWatchService } from '../service/company-watch.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  watchedStocks: CompanyStock[] = [];

  constructor(private eventEmitterService: EventEmitterService, private watchService:CompanyWatchService) { 
    this.eventEmitterService.onFirstComponentButtonClick();
    
    this.watchService.getWatchList(Number(localStorage.getItem("userId"))).subscribe(
      data=>{
        for(let val of data){
          this.watchedStocks.push(val);
        }
      }
    )
  }

  ngOnInit(): void {
  }

  remove(name:string, id:number){
    let userId = Number(localStorage.getItem("userId"));
    console.log(userId, id);
    this.watchService.removeWatchList(userId,id).subscribe();
    alert(name+ " successfully removed from the watch list!!");
    window.location.reload();

  }
}
