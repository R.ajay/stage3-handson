import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../event-emitter.service';
import { AuthServiceService } from '../service/auth-service.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  login = true;
  companies = true;
  watchlist = false
  compareperformance = false;
  logout = false;

  constructor(private eventEmitterService: EventEmitterService, private auth:AuthServiceService ) { }

  ngOnInit(): void {
    if (this.eventEmitterService.subsVar==undefined) {    
      this.eventEmitterService.subsVar = this.eventEmitterService.    
      invokeFirstComponentFunction.subscribe((name:string) => {    
        this.loginMenu();    
      });    
    }
  }

  loginMenu(){
      this.watchlist = true;
      this.compareperformance = true;
      this.logout = true;
      this.login = false;
  }
  logoutMenu(){
    if(confirm("Are you sure you want to log out?")){
      this.watchlist = false;
    this.compareperformance = false;
    this.logout = false;
    this.login = true;
    localStorage.clear();
    }
  }

}
