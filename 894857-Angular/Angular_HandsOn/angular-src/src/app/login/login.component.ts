import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { EventEmitterService } from '../event-emitter.service';
import { AuthServiceService } from '../service/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginObject = new Logins("","");

  constructor(private auth:AuthServiceService, private router: Router, private eventEmitterService: EventEmitterService ) { }

  ngOnInit(): void {
  }
  

  handleLogin(){
    this.auth.validate(this.loginObject.email,this.loginObject.password).subscribe(
      data=>{
        console.log(data)
        localStorage.setItem("userId",data.id);
        alert("Logged in successfully");
        this.router.navigate(['/dashboard']);
        // this.eventEmitterService.onFirstComponentButtonClick();
      },
      (error)=>{alert("Invalid Credentials")}
    )
    
  }

}

export class Logins{
  email:string;
  password:string;

  constructor(email:string, password:string){
    this.email = email;
    this.password = password;
  }
}