import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from './employee/employee.component';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {

  transform(employees: Employee[], searchValue: string): Employee[] {
    if(!employees || !searchValue){
      return employees;
    }
    return employees.filter(employee => employee.name.toLowerCase().includes(searchValue.toLowerCase()));
  }

}
