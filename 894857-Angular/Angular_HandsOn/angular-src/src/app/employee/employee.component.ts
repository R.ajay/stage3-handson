import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeService } from '../employee.service';



@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  dataString:string;
  errorMsg:string;
  employeeObj = new Employee(0,"","","");
  employees:Employee[] = [];
  idInitializer = 1001;
  showform = false;
  showdetails = false;
  showupdateform = false;
  searchValue:string;



  constructor(private employeeService:EmployeeService) { }


  ngOnInit(): void {
  }

  displayregisterform(){
    this.employeeObj.name ="";
    this.employeeObj.department="";
    this.employeeObj.designation="";
    this.showform = true;
    this.showdetails = false;
  }

  register(){
    let id = 0;
    let name = this.employeeObj.name;
    let department = this.employeeObj.department;
    let designation = this.employeeObj.designation;
    // this.employees.push(new Employee(id, name, department, designation));
    this.employeeService.sendEmployee(new Employee(id, name, department, designation)).subscribe(
      data=>
      {
        this.employeeObj = data;
        this.dataString = "Employee id : "+this.employeeObj.id+ ", Name: "+this.employeeObj.name+ ", Department: "+this.employeeObj.department+", Designation: "+this.employeeObj.designation;
      },
      (error)=>{this.errorMsg = error;alert(this.errorMsg)}
    )
    
    this.employeeObj.name ="";
    this.employeeObj.department="";
    this.employeeObj.designation="";

    console.log("name " +this.employeeObj.name);

    alert("Details Registered Successfully!!");
    this.showform = false;
  }

  displayEmployees(){
    this.searchValue="";
    this.showform = false;
    this.showdetails = true;
    this.employeeService.fetchEmployees().subscribe(
      data=>
      {
        this.employees = data;
      },
      (error)=>{this.errorMsg = error;alert(this.errorMsg)}
    )
  }

  delete(id:number){
    if(confirm("Are you sure? you want to delete employee details having ID "+id)){
      for(let i=0; i<this.employees.length;i++){
        if(this.employees[i].id===id){
          this.employeeService.deleteEmployees(id).subscribe(
            data=>
            {
              console.log(data);
            },
            (error)=>{this.errorMsg = error;alert(this.errorMsg)});

          this.employees.splice(i,1);
          
        }
      }
    }
  }

  update(id: number){
    for(let i=0; i<this.employees.length;i++){
      if(this.employees[i].id===id){
        this.employeeObj = this.employees[i];
        break;
      }
    }
    this.showupdateform = true;
    this.showdetails = false;    
  }

  submitupdate(){
    let id = this.employeeObj.id;
    let name = this.employeeObj.name;
    let department = this.employeeObj.department;
    let designation = this.employeeObj.designation;
    for(let i=0; i<this.employees.length;i++){
      if(this.employees[i].id===id){
        this.employeeService.updateEmployees(this.employees[i]).subscribe(
          data=>
          {
            console.log(data);
          },
          (error)=>{this.errorMsg = error;alert(this.errorMsg)});
        break;
      }
    }
    this.employeeObj.name ="";
    this.employeeObj.department="";
    this.employeeObj.designation="";
    alert("Details Updated Successfully!!");
    this.showupdateform = false;
  }


}

export class Employee{
  id:number;
  name:string;
  department:string;
  designation:string;
  constructor (id:number, name:string, department:string, designation:string){
    this.id = id;
    this.name = name;
    this.department = department;
    this.designation = designation;
  }
}
