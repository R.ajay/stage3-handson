import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Logins } from '../login/login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  private baseURL = "http://localhost:8080/login";
  private headers = new HttpHeaders ({'content-type':'application/json'});

  constructor(private http:HttpClient) { }

  validate(email:string, password:string): Observable<any>{
    let url = this.baseURL +"/validate";
    return this.http.post(url,new Logins(email,password));
  }

  getStocks(): Observable<any>{
    let url = this.baseURL +"/stock";
    return this.http.get(url);
  }

}