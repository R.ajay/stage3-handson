import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CompanyWatchService {

  
  private baseURL = "http://localhost:8080/companyStock";
  private headers = new HttpHeaders ({'content-type':'application/json'});

  constructor(private http:HttpClient) { }

  addWatchList(userId:number,companyId:number):Observable<any>{
    let url = this.baseURL + "/watch?userId="+userId+"&companyId="+companyId;
    return this.http.get(url, {responseType:'text'});
  }

  removeWatchList(userId:number,companyId:number):Observable<any>{
    let url = this.baseURL + "/remove?userId="+userId+"&companyId="+companyId;
    return this.http.get(url, {responseType:'text'});
  }
  // watchlist
  getWatchList(userId:number):Observable<any>{
    let url = this.baseURL + "/watchList?userId="+userId;
    return this.http.get(url);
  }

  getUnWatchList(userId:number):Observable<any>{
    let url = this.baseURL + "/unWatchList?userId="+userId;
    return this.http.get(url);
  }
}
