import { Component, OnInit } from '@angular/core';
import { CompanyStock } from '../company-stock';
import { EventEmitterService } from '../event-emitter.service';
import { AuthServiceService } from '../service/auth-service.service';
import { CompanyWatchService } from '../service/company-watch.service';

@Component({
  selector: 'app-dashboard-user',
  templateUrl: './dashboard-user.component.html',
  styleUrls: ['./dashboard-user.component.css']
})
export class DashboardUserComponent implements OnInit {

  watchButton = false;
  unWatchButton = false;
  stocks: CompanyStock[] = [];
  unwatchedStocks: CompanyStock[] = [];


  constructor(private auth:AuthServiceService, private eventEmitterService: EventEmitterService, private watchService:CompanyWatchService) {
    console.log("auth id="+localStorage.getItem("userId"));
    if(localStorage.getItem("userId")){
      this.watchButton = true;
      this.eventEmitterService.onFirstComponentButtonClick();
    }

    this.auth.getStocks().subscribe(
      data=>{
        for(let val of data){
          this.stocks.push(val);
        }
        
      }
    )


    this.watchService.getUnWatchList(Number(localStorage.getItem("userId"))).subscribe(
      data=>{
        for(let val of data){
          this.unwatchedStocks.push(val);
        }
      }
    )

   }


  ngOnInit(): void {
    
  }

  add(name:string, id:number){
    this.watchButton = false;
    this.unWatchButton = true;
    let userId = Number(localStorage.getItem("userId"));
    console.log(userId, id);
    this.watchService.addWatchList(userId,id).subscribe();
    alert(name+ " successfully added to the watch list!!");
    window.location.reload();
  }


}
